import { ManageBannersComponent } from './features/banners/manage-banners/manage-banners.component';

import { HttpClientModule } from '@angular/common/http';
import { RoutingModule } from './app.routing';
import { CrankyMaterialModule } from './angular-material.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Globals } from './globals';
import { TableModule } from 'ngx-easy-table';

// Directives
import { NumbersDirective } from './directives/numbers.directive';

// Services
import { LoginService } from './services/login/login.service';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';
import { DashboardService } from './services/dashboard/dashboard.service';

// Components
import { AppComponent } from './app.component';
import { LoginComponent } from './features/login/login/login.component';
import { PassRecoveryComponent } from './features/login/pass-recovery/pass-recovery.component';
import { DashboardComponent } from './features/dashboard/dashboard/dashboard.component';
import { MenuComponent } from './shared/menu/menu.component';
import { LastUsersComponent } from './features/dashboard/widgets/last-users/last-users.component';
import { LastVehiclesComponent } from './features/dashboard/widgets/last-vehicles/last-vehicles.component';
import { UtilityCounterComponent } from './features/dashboard/widgets/utility-counter/utility-counter.component';
import { BrandsComponent } from './features/brands/brands/brands.component';
import { ManageCarObjectsComponent } from './shared/manage-car-objects/manage-car-objects/manage-car-objects.component';
import { ModelsComponent } from './features/models/models/models.component';
import { MotorsComponent } from './features/motors/motors/motors.component';
import { MotorsVersionComponent } from './features/motors-version/motors-version/motors-version.component';
import { StereosComponent } from './features/stereos/stereos/stereos.component';
import { PublicContainerComponent } from './public-container/public-container.component';
import { PrivateContainerComponent } from './private-container/private-container.component';
import { SpareCategoriesCrudComponent } from './features/spare-categories/spare-categories-crud/spare-categories-crud.component';
import { ManageProductsComponent } from './shared/manage-products/manage-products/manage-products.component';
import { PlansCrudComponent } from './features/plans/plans-crud/plans-crud.component';
import { NewPlanComponent } from './features/plans/new-plan/new-plan.component';
import { BannersComponent } from './features/banners/banners/banners.component';
import { SalesListComponent } from './features/sales/sales-list/sales-list.component';
import { VehiclesOnSaleComponent } from './features/vehiclesonsale/vehicles-on-sale/vehicles-on-sale.component';
import { AdditionalServicesComponent } from './features/vehiclesonsale/additional-services/additional-services.component';
import { EssentialServicesComponent } from './features/vehiclesonsale/essential-services/essential-services.component';
import { CranesComponent } from './features/cranes/cranes/cranes.component';
import { ApproveCranesComponent } from './features/cranes/approve-cranes/approve-cranes.component';
import { EditCranesComponent } from './features/cranes/edit-cranes/edit-cranes.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PassRecoveryComponent,
    DashboardComponent,
    MenuComponent,
    LastVehiclesComponent,
    LastUsersComponent,
    UtilityCounterComponent,
    BrandsComponent,
    ManageCarObjectsComponent,
    ModelsComponent,
    MotorsComponent,
    MotorsVersionComponent,
    StereosComponent,
    PublicContainerComponent,
    PrivateContainerComponent,
    SpareCategoriesCrudComponent,
    ManageProductsComponent,
    PlansCrudComponent,
    NewPlanComponent,
    BannersComponent,
    ManageBannersComponent,
    SalesListComponent,
    VehiclesOnSaleComponent,
    NumbersDirective,
    AdditionalServicesComponent,
    EssentialServicesComponent,
    CranesComponent,
    ApproveCranesComponent,
    EditCranesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
    RoutingModule,
    CrankyMaterialModule,
    TableModule
  ],
  providers: [
    Globals,
    LoginService,
    AuthGuardService,
    AuthService,
    DashboardService
  ],
  entryComponents: [
    ManageCarObjectsComponent,
    ManageProductsComponent,
    NewPlanComponent,
    ManageBannersComponent,
    AdditionalServicesComponent,
    EssentialServicesComponent
  ],
  bootstrap: [AppComponent],
  exports: [LoginComponent]
})
export class AppModule {}
