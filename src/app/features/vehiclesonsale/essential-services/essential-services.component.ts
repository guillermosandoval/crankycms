import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

// Services
import { EasyTableConfigService } from './../../../services/easy-table-config.service';

@Component({
  selector: 'app-essential-services',
  templateUrl: './essential-services.component.html',
  styleUrls: ['./essential-services.component.css']
})
export class EssentialServicesComponent implements OnInit {
  public configuration;
  public dataTable;
  public title: string;
  public columns = [
    { title: 'Nombre del Servicio' },
    { title: 'Kilometraje' },
    { title: 'Precio' },
    { title: 'Realizado Por' },
    { title: 'Descripcion' },
    { title: 'Fecha' },
    { title: 'Meses' },
    { title: 'Imagen' }
  ];
  constructor(
    public dialogRef: MatDialogRef<EssentialServicesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.configuration = EasyTableConfigService.config;
    this.configuration.rows = 3;
    this.configuration.paginationRangeEnabled = false;
    this.dataTable = this.data.row;
    this.title = this.data.title;
  }
}
