import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EssentialServicesComponent } from './essential-services.component';

describe('EssentialServicesComponent', () => {
  let component: EssentialServicesComponent;
  let fixture: ComponentFixture<EssentialServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EssentialServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EssentialServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
