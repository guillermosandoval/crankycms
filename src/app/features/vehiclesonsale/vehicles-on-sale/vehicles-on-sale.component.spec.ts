import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclesOnSaleComponent } from './vehicles-on-sale.component';

describe('VehiclesOnSaleComponent', () => {
  let component: VehiclesOnSaleComponent;
  let fixture: ComponentFixture<VehiclesOnSaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehiclesOnSaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiclesOnSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
