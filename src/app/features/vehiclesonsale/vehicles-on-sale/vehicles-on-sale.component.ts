import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatSnackBar, MatDialog } from '@angular/material';

// Services
import { EasyTableConfigService } from './../../../services/easy-table-config.service';
import { VehiclesOnSaleService } from './../../../services/vehicles-on-sale/vehicles-on-sale.service';
import { ModelsService } from './../../../services/models/models.service';
import { BrandsService } from './../../../services/brands/brands.service';

// Components
import { AdditionalServicesComponent } from '../additional-services/additional-services.component';
import { EssentialServicesComponent } from '../essential-services/essential-services.component';

@Component({
  selector: 'app-vehicles-on-sale',
  templateUrl: './vehicles-on-sale.component.html',
  styleUrls: ['./vehicles-on-sale.component.css']
})
export class VehiclesOnSaleComponent implements OnInit {
  filteredModels: Observable<any[]>;
  filteredBrands: Observable<any[]>;
  public data;
  public configuration;
  public models: any[];
  public brands: any[];
  public loading: boolean;
  public form: FormGroup;
  public paramsMap = new Map<any, any>();
  public columns = [
    { title: 'Id del Vehiculo' },
    { title: 'Foto' },
    { title: 'Nombre del Vendedor' },
    { title: 'Marca - Modelo - Año' },
    { title: 'Vehiculo Pago' },
    { title: 'Precio' },
    { title: 'Plan del Vehiculo' },
    { title: 'Servicios' }
  ];

  constructor(
    public vehiclesOnSaleService: VehiclesOnSaleService,
    private modelsService: ModelsService,
    private brandsService: BrandsService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.configuration = EasyTableConfigService.config;
    this.configuration.rows = 10;
    this.configuration.paginationRangeEnabled = true;
    this.configuration.isLoading = true;
    this.loadData();
    this.form = new FormGroup({
      modelCtrl: new FormControl('', []),
      brandCtrl: new FormControl('', []),
      locationCtrl: new FormControl('', []),
      yearFromCtrl: new FormControl('', []),
      yearToCtrl: new FormControl('', [])
    });

    this.form.valueChanges.subscribe(val => {
      if (val['modelCtrl']) {
        this.paramsMap.set('ModelId', val['modelCtrl']['ModelId']);
      }
      if (val['modelCtrl'] === '') {
        this.paramsMap.delete('ModelId');
      }
      if (val['brandCtrl']) {
        this.paramsMap.set('BrandId', val['brandCtrl']['BrandId']);
      }
      if (val['brandCtrl'] === '') {
        this.paramsMap.delete('BrandId');
      }
      if (val['locationCtrl']) {
        this.paramsMap.set('Location', val['locationCtrl']);
      }
      if (val['locationCtrl'] === '') {
        this.paramsMap.delete('Location');
      }
      if (val['yearFromCtrl']) {
        this.paramsMap.set('YearFrom', val['yearFromCtrl']);
      }
      if (val['yearFromCtrl'] === '') {
        this.paramsMap.delete('YearFrom');
      }
      if (val['yearToCtrl']) {
        this.paramsMap.set('YearTo', val['yearToCtrl']);
      }
      if (val['yearToCtrl'] === '') {
        this.paramsMap.delete('YearTo');
      }
    });
  }

  loadData() {
    this.loading = true;
    this.vehiclesOnSaleService
      .getAllVehicleForSale()
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.data = res['body'];
          this.modelsService
            .getAllModels()
            .toPromise()
            .then(models => {
              this.models = models['body']['Models'];
              this.filteredModels = this.form
                .get('modelCtrl')
                .valueChanges.pipe(
                  startWith(''),
                  map(model =>
                    model ? this._filterModels(model) : this.models
                  )
                );

              this.brandsService
                .getAllBrands()
                .toPromise()
                .then(brands => {
                  if (brands['status'] === 200) {
                    this.brands = brands['body'];
                    this.filteredBrands = this.form
                      .get('brandCtrl')
                      .valueChanges.pipe(
                        startWith(''),
                        map(brand =>
                          brand ? this._filterBrands(brand) : this.brands
                        )
                      );
                  }
                });
            });
        } else {
          this.data = [];
        }
        this.configuration.isLoading = false;
        this.loading = false;
      });
  }

  displayFnModels(model): string | undefined {
    return model ? model.Name : undefined;
  }

  displayFnBrands(brand): string | undefined {
    return brand ? brand.Name : undefined;
  }

  private _filterModels(value) {
    if (typeof value === 'string') {
      const filterValue = value.toLowerCase();

      const found = this.models.filter(
        m => m.Name.toLowerCase().indexOf(filterValue.toLowerCase()) === 0
      );

      if (found) {
        return found;
      } else {
        return this.models;
      }
    }
  }

  private _filterBrands(value) {
    if (typeof value === 'string') {
      const filterValue = value.toLowerCase();

      const found = this.brands.filter(
        b => b.Name.toLowerCase().indexOf(filterValue.toLowerCase()) === 0
      );

      if (found) {
        return found;
      } else {
        return this.brands;
      }
    }
  }

  filterData() {
    this.vehiclesOnSaleService
      .getAllVehicleForSale(this.paramsMap)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.data = res['body'];
        } else {
          this.data = [];
        }
      });
  }

  clearFilters() {
    this.form.reset();
    this.paramsMap.clear();
    this.vehiclesOnSaleService
      .getAllVehicleForSale()
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.data = res['body'];
        } else {
          this.data = [];
        }
      });
  }

  updatePayment(row) {
    this.vehiclesOnSaleService
      .updatePayment(row.VehicleOnSaleId)
      .toPromise()
      .then(() => {
        this.snackBar.open('Información guardada correctamente', 'Aceptar');
        this.loadData();
      });
  }

  viewAdditionalServices(row) {
    this.dialog.open(AdditionalServicesComponent, {
      data: {
        title: row.Name,
        row: row.AdditionalServices
      }
    });
  }

  viewEssentialServices(row) {
    this.dialog.open(EssentialServicesComponent, {
      data: {
        title: row.Name,
        row: row.EssentialServices
      }
    });
  }
}
