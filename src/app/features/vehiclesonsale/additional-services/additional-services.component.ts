import { Component, OnInit, Inject } from '@angular/core';

// Services
import { EasyTableConfigService } from './../../../services/easy-table-config.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-additional-services',
  templateUrl: './additional-services.component.html',
  styleUrls: ['./additional-services.component.css']
})
export class AdditionalServicesComponent implements OnInit {
  public configuration;
  public dataTable;
  public title: string;
  public columns = [
    { title: 'Nombre del Servicio' },
    { title: 'Kilometraje' },
    { title: 'Precio' },
    { title: 'Realizado Por' },
    { title: 'Descripcion' },
    { title: 'Fecha' },
    { title: 'Meses' },
    { title: 'Imagen' }
  ];
  constructor(
    public dialogRef: MatDialogRef<AdditionalServicesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.configuration = EasyTableConfigService.config;
    this.configuration.rows = 3;
    this.configuration.paginationRangeEnabled = false;
    this.dataTable = this.data.row;
    this.title = this.data.title;
  }
}
