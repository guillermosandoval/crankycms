import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// Services
import { EasyTableConfigService } from './../../../services/easy-table-config.service';
import { ModelsService } from './../../../services/models/models.service';
// Components
import { ManageCarObjectsComponent } from './../../../shared/manage-car-objects/manage-car-objects/manage-car-objects.component';

@Component({
  selector: 'app-models',
  templateUrl: './models.component.html',
  styleUrls: ['./models.component.css']
})
export class ModelsComponent implements OnInit {
  public selected;
  public data;
  public configuration;

  public loading: boolean;
  public recordId: string;
  public recordName: string;
  public recordNameParent: string;
  public newRecordName: string;
  public form: any;
  public brands: any;

  public modal = false;

  public columns = [
    { key: 'ModelId', title: 'Id de Modelo' },
    { key: 'BrandName', title: 'Nombre de la Marca' },
    { key: 'Name', title: 'Nombre del Modelo' },
    { key: '', title: '' }
  ];

  constructor(
    private modelsService: ModelsService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.configuration = EasyTableConfigService.config;
    this.configuration.isLoading = true;
    this.configuration.rows = 10;
    this.configuration.paginationRangeEnabled = true;
    this.loadData();
    this.form = new FormGroup({
      recordname: new FormControl('', [
        Validators.required,
        Validators.minLength(2)
      ]),
      recordnameparent: new FormControl('', [
        Validators.required,
        Validators.minLength(2)
      ])
    });
  }

  loadData() {
    this.loading = true;
    this.modelsService
      .getAllModels()
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.data = res['body']['Models'];
          this.brands = res['body']['Brands'];
        } else {
          this.data = [];
        }
        this.configuration.isLoading = false;
        this.loading = false;
      });
  }

  editRecord(row) {
    this.recordId = row['ModelId'];
    this.recordName = row['Name'];
    this.recordNameParent = row['BrandName'];
    const dialogRef = this.dialog.open(ManageCarObjectsComponent, {
      width: '400px',
      height: '300px',
      data: { action: 'edit', recordName: this.recordName, newRecordName: '' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result['update'] === true) {
        this.updateRecord(result['value']);
      }
    });
  }

  confirmDelete(row) {
    this.recordId = row['ModelId'];
    this.recordName = row['Name'];
    this.recordNameParent = row['BrandName'];
    const dialogRef = this.dialog.open(ManageCarObjectsComponent, {
      width: '400px',
      height: '200px',
      data: { action: 'delete', recordName: this.recordName, newRecordName: '' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result['update'] === true) {
        this.deleteRecord();
      }
    });
  }

  saveRecord() {
    this.modelsService
      .addModel(
        this.form.get('recordname').value,
        this.form.get('recordnameparent').value
      )
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open('Información guardada correctamente', 'Aceptar');
          this.form.get('recordname').setValue('');
          this.form.get('recordnameparent').setValue('');
          this.form.markAsPristine();
          this.form.markAsUntouched();
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }

  updateRecord(newRecordName: string) {
    this.modelsService
      .updateModel(newRecordName, this.recordId)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open(
            'Información actualizada correctamente',
            'Aceptar'
          );
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }

  deleteRecord() {
    this.modelsService
      .deleteModel(this.recordId)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open('Registro eliminado correctamente', 'Aceptar');
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }
}
