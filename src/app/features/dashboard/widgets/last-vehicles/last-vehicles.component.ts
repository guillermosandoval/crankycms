import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard/dashboard.service';

@Component({
  selector: 'app-last-vehicles',
  templateUrl: './last-vehicles.component.html',
  styleUrls: ['./last-vehicles.component.css']
})
export class LastVehiclesComponent implements OnInit {
  public vehicleData: any;

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    this.dashboardService
      .getLastTenVehicles()
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.vehicleData = res['body'];
        } else {
          this.vehicleData = [];
        }
      });
  }
}
