import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard/dashboard.service';

@Component({
  selector: 'app-utility-counter',
  templateUrl: './utility-counter.component.html',
  styleUrls: ['./utility-counter.component.css']
})
export class UtilityCounterComponent implements OnInit {

  public utilityData: any;

  constructor(private dashboardService: DashboardService, public router: Router) { }

  ngOnInit() {
    this.dashboardService
    .counterVehiculeUtility()
    .toPromise()
    .then(res => {
      if (res['status'] === 200) {
        this.utilityData = res['body'];
      } else {
        this.utilityData = [];
      }
    });
  }

  navigateTo(route) {
    this.router.navigate([route]);
  }

}
