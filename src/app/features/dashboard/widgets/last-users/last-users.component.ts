import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard/dashboard.service';

@Component({
  selector: 'app-last-users',
  templateUrl: './last-users.component.html',
  styleUrls: ['./last-users.component.css']
})
export class LastUsersComponent implements OnInit {

  public userData: any;

  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.dashboardService
      .getLastTenUsers()
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.userData = res['body'];
        } else {
          this.userData = [];
        }
      });
  }


}
