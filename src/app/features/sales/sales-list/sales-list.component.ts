import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';

// Services
import { EasyTableConfigService } from './../../../services/easy-table-config.service';
import { SalesServices } from './../../../services/sales/sales.service';

@Component({
  selector: 'app-sales-list',
  templateUrl: './sales-list.component.html',
  styleUrls: ['./sales-list.component.css']
})
export class SalesListComponent implements OnInit {
  public selected;
  public data;
  public configuration;

  public loading: boolean;

  public modal = false;

  public columns = [
    { key: 'SaleId', title: '# Venta' },
    { key: 'SparePhoto', title: 'Foto de Producto' },
    { key: 'SellerName', title: 'Vendedor' },
    { key: 'BuyerName', title: 'Comprador' },
    { key: 'SpareName', title: 'Producto' },
    { key: 'BrandName', title: 'Marca' },
    { key: 'ModelName', title: 'Modelo' },
    { key: 'Price', title: 'Precio' },
    { key: '', title: '' }
  ];

  constructor(
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public salesServices: SalesServices
  ) {}

  ngOnInit() {
    this.configuration = EasyTableConfigService.config;
    this.configuration.isLoading = true;
    this.configuration.rows = 10;
    this.configuration.paginationRangeEnabled = true;
    this.loadData();
  }

  loadData() {
    this.loading = true;
    this.salesServices
      .getAllSales()
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.data = res['body'];
        } else {
          this.data = [];
        }
        this.configuration.isLoading = false;
        this.loading = false;
      });
  }

  selectChanged(e, id) {
    var value = e.target.value;
    this.loading = true;
    this.salesServices
      .updateSale(id, value)
      .toPromise()
      .then(res => {
        this.loadData();
        this.snackBar.open('Información guardada correctamente', 'Aceptar');
        this.configuration.isLoading = false;
        this.loading = false;
      });
  }
}
