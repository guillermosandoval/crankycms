import { AuthService } from './../../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// Services
import { LoginService } from './../../../services/login/login.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loading;
  public year: any;
  public form: any;

  constructor(
    private loginService: LoginService,
    private router: Router,
    public authService: AuthService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.year = new Date().getFullYear();
    this.form = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ])
    });
  }

  async login() {
    this.loading = true;
    await this.loginService
      .authAdmin(
        this.form.get('username').value,
        this.form.get('password').value
      )
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          sessionStorage.setItem('token', JSON.stringify(res));
          this.authService.isLogged = true;
          this.authService.logInformation = res;
          this.router.navigate(['/dashboard']);
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
        this.loading = false;
      });
  }
}
