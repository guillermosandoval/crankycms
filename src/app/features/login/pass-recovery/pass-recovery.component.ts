import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

// Services
import { LoginService } from './../../../services/login/login.service';

@Component({
  selector: 'app-pass-recovery',
  templateUrl: './pass-recovery.component.html',
  styleUrls: ['./pass-recovery.component.css']
})
export class PassRecoveryComponent implements OnInit {
  public loading;
  public form: any;

  constructor(
    private loginService: LoginService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      param: new FormControl('', [Validators.required])
    });
  }

  sendEmailPassword() {
    this.loading = true;
    this.loginService
      .passwordRecovery(this.form.get('param').value)
      .toPromise()
      .then(res => {
        this.snackBar.open(
          res['body'],
          'Aceptar'
        );
        this.loading = false;
      });
  }
}
