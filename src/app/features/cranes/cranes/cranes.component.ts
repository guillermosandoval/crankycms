import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';

// Services
import { EasyTableConfigService } from './../../../services/easy-table-config.service';
import { CranesService } from './../../../services/cranes/cranes.service';

// Components
import { ManageCarObjectsComponent } from './../../../shared/manage-car-objects/manage-car-objects/manage-car-objects.component';

@Component({
  selector: 'app-cranes',
  templateUrl: './cranes.component.html',
  styleUrls: ['./cranes.component.css']
})
export class CranesComponent implements OnInit {
  public selected;
  public data;
  public configuration;
  public loading: boolean;
  public recordId: string;
  public recordName: string;
  public newRecordName: string;
  public modal = false;

  public columns = [
    { key: 'CraneId', title: 'Id de la Grua' },
    { key: 'IdentificationPhoto', title: 'Foto' },
    { key: 'UserPhoto', title: 'Foto Id Usuario' },
    { key: 'Names', title: 'Nombre Completo' },
    { key: 'IdentificationNumber', title: 'Cedula' },
    { key: 'Rif', title: 'Rif' },
    { key: 'Phone', title: 'Datos Contacto' },
    { key: 'CranePhoto', title: 'Foto Grua' },
    { key: 'BrandName', title: 'Grua' },
    { key: 'Plate', title: 'Placa' },
    { key: '', title: '' }
  ];

  constructor(
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public cranesService: CranesService
  ) {}

  ngOnInit() {
    this.configuration = EasyTableConfigService.config;
    this.configuration.isLoading = true;
    this.configuration.rows = 10;
    this.configuration.paginationRangeEnabled = true;
    this.loadData();
  }

  loadData() {
    this.loading = true;
    this.cranesService
      .getAllPendingCranes()
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.data = res['body'];
        } else {
          this.data = [];
        }
        this.configuration.isLoading = false;
        this.loading = false;
      });
  }

  approveCrane(craneId) {
    this.cranesService
      .updateCraneStatus(craneId, 2)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open('Grua aprobada correctamente', 'Aceptar');
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }

  rejectCrane(craneId) {
    this.cranesService
      .updateCraneStatus(craneId, 5)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open('Grua rechazada correctamente', 'Aceptar');
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }

  confirmDelete(row) {
    this.recordName = row['Name'];
    const dialogRef = this.dialog.open(ManageCarObjectsComponent, {
      width: '400px',
      height: '200px',
      data: { action: 'delete', recordName: this.recordName, newRecordName: '' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result['update'] === true) {
        this.deleteRecord(row['CraneId']);
      }
    });
  }
  deleteRecord(craneId) {
    this.cranesService
      .deleteCrane(craneId)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open('Grua eliminada correctamente', 'Aceptar');
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }
}
