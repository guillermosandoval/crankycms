import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveCranesComponent } from './approve-cranes.component';

describe('ApproveCranesComponent', () => {
  let component: ApproveCranesComponent;
  let fixture: ComponentFixture<ApproveCranesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveCranesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveCranesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
