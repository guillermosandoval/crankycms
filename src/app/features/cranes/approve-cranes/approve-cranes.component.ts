import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';

// Services
import { EasyTableConfigService } from './../../../services/easy-table-config.service';
import { CranesService } from './../../../services/cranes/cranes.service';

// Components
import { ManageCarObjectsComponent } from './../../../shared/manage-car-objects/manage-car-objects/manage-car-objects.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-approve-cranes',
  templateUrl: './approve-cranes.component.html',
  styleUrls: ['./approve-cranes.component.css']
})
export class ApproveCranesComponent implements OnInit, OnDestroy {
  public selected;
  public data;
  public configuration;
  public loading: boolean;
  public recordId: string;
  public recordName: string;
  public newRecordName: string;
  public modal = false;

  public columns = [
    { key: 'Id', title: 'Id de la Grua' },
    { key: 'UserPhoto', title: 'Foto Usuario' },
    { key: 'UserPhoto', title: 'Foto Id Usuario' },
    { key: 'Name', title: 'Nombre Completo' },
    { key: 'UserId', title: 'Cedula' },
    { key: 'PersonalUserId', title: 'Rif' },
    { key: 'Contact', title: 'Datos Contacto' },
    { key: 'CranePhoto', title: 'Foto Grua' },
    { key: 'CarDetails', title: 'Grua' },
    { key: 'CarId', title: 'Placa' },
    { key: 'Payed', title: 'Pagado' },
    { key: '', title: '' }
  ];

  constructor(
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public cranesService: CranesService,
    private router: Router
  ) {}

  ngOnInit() {
    this.configuration = EasyTableConfigService.config;
    this.configuration.isLoading = true;
    this.configuration.rows = 10;
    this.configuration.paginationRangeEnabled = true;
    this.loadData();
  }

  ngOnDestroy(): void {
    this.data = null;
  }

  loadData() {
    this.loading = true;
    this.cranesService
      .getAllApprovedCranes()
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.data = res['body'];
        } else {
          this.data = [];
        }
        this.configuration.isLoading = false;
        this.loading = false;
      });
  }

  confirmDelete(row) {
    this.recordName =  `${row['Names']} ${row['LastNames']} ?`;
    const dialogRef = this.dialog.open(ManageCarObjectsComponent, {
      width: '400px',
      height: '200px',
      data: { action: 'delete', recordName: this.recordName, newRecordName: '' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result['update'] === true) {
        this.deleteRecord(row['CraneId']);
      }
    });
  }

  deleteRecord(craneId) {
    this.cranesService
      .deleteCrane(craneId)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open('Grua eliminada correctamente', 'Aceptar');
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }

  updatePayment(row, value) {
    const data = new Map<any, any>();
    data.set('CraneId', row['CraneId']);

    if (value) {
      const membershipInit = new Date().toJSON().slice(0, 10);

      data.set('MemershipInit', membershipInit);

      const membershipEnd = new Date();

      const endDate = new Date(
        membershipEnd.setMonth(
          membershipEnd.getMonth() + parseInt(row['CranesPlanRenewal'], 0)
        )
      )
        .toJSON()
        .slice(0, 10);

      data.set('MemershipEnd', endDate);

      data.set('IsPayed', 1);
    } else {
      data.set('MemershipInit', null);
      data.set('MemershipEnd', null);
      data.set('IsPayed', 0);
    }

    this.cranesService
      .updateCraneMembership(data)
      .toPromise()
      .then(() => {
        this.snackBar.open('Información guardada correctamente', 'Aceptar');
        this.loadData();
      });
  }

  editRecord(row) {
    sessionStorage.setItem('row', JSON.stringify(row));
    this.router.navigate(['/edit-cranes']);
  }
}
