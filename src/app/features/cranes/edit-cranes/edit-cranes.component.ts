import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { startWith, map } from 'rxjs/operators';

// Services
import { BrandsService } from './../../../services/brands/brands.service';
import { ModelsService } from './../../../services/models/models.service';
import { CranesService } from './../../../services/cranes/cranes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-cranes',
  templateUrl: './edit-cranes.component.html',
  styleUrls: ['./edit-cranes.component.css']
})
export class EditCranesComponent implements OnInit, OnDestroy {
  form: any;
  loading: boolean;
  filteredBrands;
  brands;
  filteredModels;
  models;
  rowData;
  urlIdentificationPhoto;
  photoIdentificationPhoto;
  urlUserPhoto;
  photoUserPhoto;
  urlCranePhoto;
  photoCranePhoto;
  public paramsMap = new Map<any, any>();
  constructor(
    private brandsService: BrandsService,
    private modelsService: ModelsService,
    public snackBar: MatSnackBar,
    private cranesService: CranesService,
    private router: Router
  ) {}

  ngOnInit() {
    this.rowData = JSON.parse(sessionStorage.getItem('row'));
    this.generateForm();
    this.loadData();
  }

  ngOnDestroy(): void {
    sessionStorage.removeItem('row');
  }

  setFieldValues() {
    this.form.get('Names').setValue(this.rowData['Names']);

    this.form.get('LastNames').setValue(this.rowData['LastNames']);

    this.form
      .get('IdentificationNumber')
      .setValue(this.rowData['IdentificationNumber']);

    this.urlIdentificationPhoto = this.rowData['IdentificationPhoto'];
    this.urlUserPhoto = this.rowData['UserPhoto'];
    this.urlCranePhoto = this.rowData['CranePhoto'];

    this.form.get('Rif').setValue(this.rowData['Rif']);

    this.form.get('Phone').setValue(this.rowData['Phone']);

    this.form.get('Email').setValue(this.rowData['Email']);

    this.form.get('BrandId').setValue({
      BrandId: this.rowData['BrandId'],
      Name: this.rowData['BrandName']
    });
    this.form.get('ModelId').setValue({
      ModelId: this.rowData['ModelId'],
      Name: this.rowData['ModelName']
    });

    this.form.get('Plate').setValue(this.rowData['Plate']);
  }

  generateForm() {
    this.form = new FormGroup({
      Names: new FormControl('', [Validators.required]),
      LastNames: new FormControl('', [Validators.required]),
      IdentificationNumber: new FormControl('', [Validators.required]),
      IdentificationPhoto: new FormControl('', []),
      Rif: new FormControl('', []),
      Phone: new FormControl('', [Validators.required]),
      Email: new FormControl('', [Validators.required]),
      BrandId: new FormControl('', [Validators.required]),
      ModelId: new FormControl('', [Validators.required]),
      UserPhoto: new FormControl('', []),
      CranePhoto: new FormControl('', []),
      Plate: new FormControl('', [Validators.required])
    });

    this.form.valueChanges.subscribe(val => {
      for (const property in val) {
        if (typeof val[property] === 'object') {
          switch (property) {
            case 'BrandId':
              if (
                this.form.get('BrandId').value &&
                this.form.get('BrandId').value['BrandId'] !==
                  this.rowData['BrandId']
              ) {
                this.paramsMap.set(
                  property,
                  this.form.get(property).value['BrandId']
                );
              }

              break;
            case 'ModelId':
              if (
                this.form.get('ModelId').value &&
                this.form.get('ModelId').value['ModelId'] !==
                  this.rowData['ModelId']
              ) {
                this.paramsMap.set(
                  property,
                  this.form.get(property).value['ModelId']
                );
              }

              break;
          }
        } else {
          if (
            property !== 'IdentificationPhoto' &&
            property !== 'UserPhoto' &&
            property !== 'CranePhoto'
          ) {
            if (property === 'Rif') {
              if (val[property] !== this.rowData[property]) {
                this.paramsMap.set(property, val[property]);
              }
            }
            if (
              val.hasOwnProperty(property) &&
              val[property] !== this.rowData[property]
            ) {
              if (val[property]) {
                this.paramsMap.set(property, val[property]);
              }
            }
          }
        }
      }
    });
  }

  loadData() {
    this.loading = true;
    this.modelsService
      .getAllModels()
      .toPromise()
      .then(models => {
        this.models = models['body']['Models'];
        this.filteredModels = this.form.get('ModelId').valueChanges.pipe(
          startWith(''),
          map(model => (model ? this._filterModels(model) : this.models))
        );

        this.brandsService
          .getAllBrands()
          .toPromise()
          .then(brands => {
            if (brands['status'] === 200) {
              this.brands = brands['body'];
              this.loading = false;
              this.filteredBrands = this.form.get('BrandId').valueChanges.pipe(
                startWith(''),
                map(brand => (brand ? this._filterBrands(brand) : this.brands))
              );
            }

            this.setFieldValues();
          });
      });
  }

  private _filterModels(value) {
    if (typeof value === 'string') {
      const filterValue = value.toLowerCase();

      const found = this.models.filter(
        m => m.Name.toLowerCase().indexOf(filterValue.toLowerCase()) === 0
      );

      if (found) {
        return found;
      } else {
        return this.models;
      }
    }
  }

  private _filterBrands(value) {
    if (typeof value === 'string') {
      const filterValue = value.toLowerCase();

      const found = this.brands.filter(
        b => b.Name.toLowerCase().indexOf(filterValue.toLowerCase()) === 0
      );

      if (found) {
        return found;
      } else {
        return this.brands;
      }
    }
  }

  displayFnModels(model): string | undefined {
    return model ? model.Name : undefined;
  }

  displayFnBrands(brand): string | undefined {
    return brand ? brand.Name : undefined;
  }

  inputChanged(event: any) {
    this.readThis(event.target);
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      // tslint:disable-next-line:no-shadowed-variable
      reader.onload = (evt: any) => {
        this.urlCranePhoto = evt.target.result;
        this.form.get('CranePhoto').setValidators(Validators.required);
        this.form.get('CranePhoto').updateValueAndValidity();
        this.paramsMap.set('CranePhoto', this.urlCranePhoto);
      };

      reader.readAsDataURL(event.target.files[0]);
    }
  }

  readThis(inputValue: any): void {
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = () => {
      this.photoCranePhoto = myReader.result.toString().split(',');
      this.photoCranePhoto = this.photoCranePhoto[1];
    };
    myReader.readAsDataURL(file);
  }

  userPhotoInputChanged(event: any) {
    this.readUserPhoto(event.target);
    if (event.target.files && event.target.files[0]) {
      const readerUserPhoto = new FileReader();

      // tslint:disable-next-line:no-shadowed-variable
      readerUserPhoto.onload = (evt: any) => {
        this.urlUserPhoto = evt.target.result;
        this.form.get('UserPhoto').setValidators(Validators.required);
        this.form.get('UserPhoto').updateValueAndValidity();
        this.paramsMap.set('UserPhoto', this.urlUserPhoto);
      };

      readerUserPhoto.readAsDataURL(event.target.files[0]);
    }
  }

  readUserPhoto(inputValue) {
    const file: File = inputValue.files[0];
    const readerUserPhoto: FileReader = new FileReader();

    readerUserPhoto.onloadend = () => {
      this.photoUserPhoto = readerUserPhoto.result.toString().split(',');
      this.photoUserPhoto = this.photoUserPhoto[1];
    };
    readerUserPhoto.readAsDataURL(file);
  }

  idPhotoInputChanged(event: any) {
    this.readIdPhoto(event.target);
    if (event.target.files && event.target.files[0]) {
      const readerIdPhoto = new FileReader();

      // tslint:disable-next-line:no-shadowed-variable
      readerIdPhoto.onload = (evt: any) => {
        this.urlIdentificationPhoto = evt.target.result;
        this.form.get('IdentificationPhoto').setValidators(Validators.required);
        this.form.get('IdentificationPhoto').updateValueAndValidity();
        this.paramsMap.set('IdentificationPhoto', this.urlIdentificationPhoto);
      };

      readerIdPhoto.readAsDataURL(event.target.files[0]);
    }
  }

  readIdPhoto(inputValue) {
    const file: File = inputValue.files[0];
    const readerIdPhoto: FileReader = new FileReader();

    readerIdPhoto.onloadend = () => {
      this.photoIdentificationPhoto = readerIdPhoto.result
        .toString()
        .split(',');
      this.photoIdentificationPhoto = this.photoIdentificationPhoto[1];
    };
    readerIdPhoto.readAsDataURL(file);
  }

  guardarCambios() {
    this.paramsMap.set('CraneId', this.rowData['CraneId']);
    this.cranesService
      .updateCrane(this.paramsMap)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.snackBar.open('Grua actualizada con éxito', 'Aceptar');
          this.router.navigate(['cranes-approved']);
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }
}
