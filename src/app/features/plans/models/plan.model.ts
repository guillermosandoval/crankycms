export interface PlanModel {
  planId?: any;
  name: any;
  typeOfPost: any;
  maxPhotos: any;
  price: any;
  featured: any;
}
