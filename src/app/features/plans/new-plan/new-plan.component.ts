import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PlanModel } from '../models/plan.model';

@Component({
  selector: 'app-new-plan',
  templateUrl: './new-plan.component.html',
  styleUrls: ['./new-plan.component.css']
})
export class NewPlanComponent implements OnInit {
  public form: any;
  newData: PlanModel;

  constructor(
    public dialogRef: MatDialogRef<NewPlanComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      Name: new FormControl('', [Validators.required]),
      TypeOfPost: new FormControl('', [Validators.required]),
      MaxPhotos: new FormControl('', [Validators.required]),
      Price: new FormControl('', [Validators.required]),
      Featured: new FormControl('', [])
    });
  }

  close(action?: boolean): void {
    if (this.data['action'] !== 'delete' && action === true) {
      const isFeatured = !this.form.get('Featured').value ? false : true;
      const newData: PlanModel = {
        name: this.form.get('Name').value,
        typeOfPost: this.form.get('TypeOfPost').value,
        maxPhotos: this.form.get('MaxPhotos').value,
        price: this.form.get('Price').value,
        featured: isFeatured
      };

      if (this.data.action === 'edit') {
        newData.planId = this.data.row.PlanId;
      }

      this.dialogRef.close(newData);
    } else {
      this.dialogRef.close(action);
    }
  }
}
