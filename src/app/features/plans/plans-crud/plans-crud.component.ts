import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// Services
import { EasyTableConfigService } from './../../../services/easy-table-config.service';
import { PlansService } from 'src/app/services/plans/plans.service';
import { NewPlanComponent } from '../new-plan/new-plan.component';
import { PlanModel } from '../models/plan.model';

@Component({
  selector: 'app-plans-crud',
  templateUrl: './plans-crud.component.html',
  styleUrls: ['./plans-crud.component.css']
})
export class PlansCrudComponent implements OnInit {
  public selected;
  public data;
  public configuration;

  public loading: boolean;
  public recordId: string;
  public recordName: string;
  public newRecordName: string;
  public icon: string;
  public newIcon: string;
  public form: any;
  public newData: PlanModel;

  public modal = false;

  public columns = [
    { key: 'Name', title: 'Nombre del Plan' },
    { key: 'TypeOfPost', title: 'Tipo de Post' },
    { key: 'MaxPhotos', title: 'Max. Fotos' },
    { key: 'Price', title: 'Precio' },
    { key: 'Featured', title: 'Destacado' },
    { key: '', title: '' }
  ];

  constructor(
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public planService: PlansService
  ) {}

  ngOnInit() {
    this.configuration = EasyTableConfigService.config;
    this.configuration.isLoading = true;
    this.configuration.rows = 10;
    this.configuration.paginationRangeEnabled = true;
    this.loadData();
    this.form = new FormGroup({
      recordname: new FormControl('', [
        Validators.required,
        Validators.minLength(2)
      ]),
      icon: new FormControl('', [Validators.required])
    });
  }

  openNewPlan() {
    const dialogRef = this.dialog.open(NewPlanComponent, {
      width: '600px',
      height: '300px',
      data: { action: 'create', row: {} }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.newData = result;
        this.saveRecord();
      }
    });
  }

  loadData() {
    this.loading = true;
    this.planService
      .getAllplans()
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.data = res['body'];
        } else {
          this.data = [];
        }
        this.configuration.isLoading = false;
        this.loading = false;
      });
  }

  editRecord(row) {
    this.recordId = row['StereoId'];
    this.recordName = row['Name'];
    this.icon = row['Icon'];
    const dialogRef = this.dialog.open(NewPlanComponent, {
      width: '600px',
      height: '300px',
      data: {
        action: 'edit',
        row
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.newData = result;
        this.updateRecord();
      }
    });
  }

  confirmDelete(row) {
    this.recordId = row['PlanId'];
    this.recordName = row['Name'];
    const dialogRef = this.dialog.open(NewPlanComponent, {
      width: '400px',
      height: '200px',
      data: { action: 'delete', recordName: this.recordName }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.deleteRecord();
      }
    });
  }

  saveRecord() {
    this.planService
      .addPlan(this.newData)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open('Información guardada correctamente', 'Aceptar');
          this.form.reset();
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }

  updateRecord() {
    this.planService
      .updatePlan(this.newData)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open(
            'Información actualizada correctamente',
            'Aceptar'
          );
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }

  deleteRecord() {
    this.planService
      .deletePlan(this.recordId)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open('Registro eliminado correctamente', 'Aceptar');
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }
}
