import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlansCrudComponent } from './plans-crud.component';

describe('PlansCrudComponent', () => {
  let component: PlansCrudComponent;
  let fixture: ComponentFixture<PlansCrudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlansCrudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlansCrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
