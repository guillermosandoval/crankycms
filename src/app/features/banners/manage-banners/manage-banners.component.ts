import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-manage-banners',
  templateUrl: './manage-banners.component.html',
  styleUrls: ['./manage-banners.component.css']
})
export class ManageBannersComponent implements OnInit {
  public form: any;
  url: any;
  photoData: any;
  constructor(
    public dialogRef: MatDialogRef<ManageBannersComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      url: new FormControl('', []),
      photo: new FormControl('', [])
    });
    this.url = this.data.photo;
  }

  close(action?: boolean): void {
    if (this.data.action === 'edit' && action === true) {
      const response = {
        photo: null,
        url: this.form.get('url').value
      };

      if (this.photoData) {
        response.photo = this.photoData;
      }
      this.dialogRef.close(response);
    } else {
      this.dialogRef.close(action);
    }
  }

  setFieldMandatory() {
    this.form.get('url').setValidators([Validators.required]);
    this.form.get('url').updateValueAndValidity();
  }

  inputChangedModal(event: any) {
    this.form.get('photo').setValidators([Validators.required]);
    this.form.get('photo').updateValueAndValidity();
    this.readThis(event.target);
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      reader.onload = (ev: any) => {
        this.url = ev.target.result;
      };

      reader.readAsDataURL(event.target.files[0]);
    }
  }

  readThis(inputValue: any): void {
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = e => {
      this.photoData = myReader.result.toString().split(',');
      this.photoData = this.photoData[1];
    };
    myReader.readAsDataURL(file);
  }
}
