import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// Services
import { EasyTableConfigService } from './../../../services/easy-table-config.service';
import { BannersService } from 'src/app/services/banners/banners.service';
import { ManageProductsComponent } from 'src/app/shared/manage-products/manage-products/manage-products.component';
import { ManageBannersComponent } from '../manage-banners/manage-banners.component';

@Component({
  selector: 'app-banners',
  templateUrl: './banners.component.html',
  styleUrls: ['./banners.component.css']
})
export class BannersComponent implements OnInit {
  public selected;
  public data;
  public configuration;
  public url;
  public photoData;
  public loading: boolean;
  public photoUrl: string;
  public recordName: string;
  public recordId: any;
  public newRecordName: string;
  public icon: string;
  public newIcon: string;
  public form: any;

  public modal = false;

  public columns = [
    { key: 'BannerId', title: 'Id del banner' },
    { key: 'Photo', title: 'Foto del banner' },
    { key: 'Url', title: 'Url del Banner' },
    { key: '', title: '' }
  ];

  constructor(
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public bannersService: BannersService
  ) {}

  ngOnInit() {
    this.configuration = EasyTableConfigService.config;
    this.configuration.isLoading = true;
    this.configuration.rows = 10;
    this.configuration.paginationRangeEnabled = true;
    this.loadData();
    this.form = new FormGroup({
      url: new FormControl('', [Validators.required]),
      photo: new FormControl('', [Validators.required])
    });
  }

  loadData() {
    this.loading = true;
    this.bannersService
      .getAllBanners()
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.data = res['body'];
        } else {
          this.data = [];
        }
        this.configuration.isLoading = false;
        this.loading = false;
      });
  }

  saveRecord() {
    this.bannersService
      .addBanner(this.photoData, this.form.get('url').value)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.url = null;
          this.snackBar.open('Información guardada correctamente', 'Aceptar');
          this.form.reset();
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }

  inputChanged(event: any) {
    this.readThis(event.target);
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      reader.onload = (ev: any) => {
        this.url = ev.target.result;
      };

      reader.readAsDataURL(event.target.files[0]);
    }
  }

  readThis(inputValue: any): void {
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = e => {
      this.photoData = myReader.result.toString().split(',');
      this.photoData = this.photoData[1];
    };
    myReader.readAsDataURL(file);
  }

  editRecord(row) {
    this.photoUrl = row['Url'];
    this.photoData = row['Photo'];
    this.recordId = row['BannerId'];
    const dialogRef = this.dialog.open(ManageBannersComponent, {
      width: '600px',
      height: '400px',
      data: {
        action: 'edit',
        url: row['Url'],
        photo: row['Photo']
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (typeof result !== 'boolean') {
        const response = {
          url: result['url'],
          photo: ''
        };

        if (result['photo']) {
          response.photo = result['photo'];
        }
        this.updateRecord(response);
      }
    });
  }

  updateRecord(newRecordName) {
    this.bannersService
      .updateBanners(newRecordName.photo, newRecordName.url, this.recordId)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.data = [];
          this.loadData();
          this.snackBar.open(
            'Información actualizada correctamente',
            'Aceptar'
          );
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }

  confirmDelete(row) {
    this.recordId = row['BannerId'];
    const dialogRef = this.dialog.open(ManageBannersComponent, {
      width: '400px',
      height: '200px',
      data: { action: 'delete', recordId: this.recordId }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.deleteRecord();
      }
    });
  }

  deleteRecord() {
    this.bannersService
      .deleteBanner(this.recordId)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open('Registro eliminado correctamente', 'Aceptar');
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }
}
