import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpareCategoriesCrudComponent } from './spare-categories-crud.component';

describe('SpareCategoriesCrudComponent', () => {
  let component: SpareCategoriesCrudComponent;
  let fixture: ComponentFixture<SpareCategoriesCrudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpareCategoriesCrudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpareCategoriesCrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
