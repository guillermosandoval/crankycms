import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// Services
import { EasyTableConfigService } from './../../../services/easy-table-config.service';
import { SpareCategoriesService } from './../../../services/spare-categories/spare-categories.service';
import { ManageProductsComponent } from 'src/app/shared/manage-products/manage-products/manage-products.component';

@Component({
  selector: 'app-spare-categories-crud',
  templateUrl: './spare-categories-crud.component.html',
  styleUrls: ['./spare-categories-crud.component.css']
})
export class SpareCategoriesCrudComponent implements OnInit {
  public selected;
  public data;
  public configuration;

  public loading: boolean;
  public recordId: string;
  public recordName: string;
  public newRecordName: string;
  public icon: string;
  public newIcon: string;
  public form: any;

  public modal = false;

  public columns = [
    { key: 'Icon', title: 'Icono de Categoria' },
    { key: 'Name', title: 'Nombre de la Categoria' },
    { key: '', title: '' }
  ];

  constructor(
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    public spareCategoriesService: SpareCategoriesService
  ) {}

  ngOnInit() {
    this.configuration = EasyTableConfigService.config;
    this.configuration.isLoading = true;
    this.configuration.rows = 10;
    this.configuration.paginationRangeEnabled = true;
    this.loadData();
    this.form = new FormGroup({
      recordname: new FormControl('', [
        Validators.required,
        Validators.minLength(2)
      ]),
      icon: new FormControl('', [Validators.required])
    });
  }

  loadData() {
    this.loading = true;
    this.spareCategoriesService
      .getAllSpareCategories()
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.data = res['body'];
        } else {
          this.data = [];
        }
        this.configuration.isLoading = false;
        this.loading = false;
      });
  }

  editRecord(row) {
    this.recordId = row['SpareCategoryId'];
    this.recordName = row['Name'];
    this.icon = row['Icon'];
    const dialogRef = this.dialog.open(ManageProductsComponent, {
      width: '400px',
      height: '300px',
      data: {
        action: 'edit',
        recordName: this.recordName,
        newRecordName: '',
        icon: this.icon,
        newIcon: ''
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result['update'] === true) {
        this.updateRecord(result['recordname'], result['icon']);
      }
    });
  }

  updateRecord(newRecordName: string, icon: string) {
    this.spareCategoriesService
      .updateSpareCategory(newRecordName, icon, this.recordId)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open(
            'Información actualizada correctamente',
            'Aceptar'
          );
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }

  confirmDelete(row) {
    this.recordId = row['SpareCategoryId'];
    this.recordName = row['Name'];
    const dialogRef = this.dialog.open(ManageProductsComponent, {
      width: '400px',
      height: '200px',
      data: { action: 'delete', recordName: this.recordName, newRecordName: '' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result['update'] === true) {
        this.deleteRecord();
      }
    });
  }

  saveRecord() {
    this.spareCategoriesService
      .addSpareCategory(
        this.form.get('recordname').value,
        this.form.get('icon').value
      )
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open('Información guardada correctamente', 'Aceptar');
          this.form.reset();
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }

  deleteRecord() {
    this.spareCategoriesService
      .deleteSpareCategory(this.recordId)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open('Registro eliminado correctamente', 'Aceptar');
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }
}
