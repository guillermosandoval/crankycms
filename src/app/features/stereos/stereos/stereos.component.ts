import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// Services
import { EasyTableConfigService } from './../../../services/easy-table-config.service';
import { StereosService } from './../../../services/stereos/stereos.service';

// Components
import { ManageCarObjectsComponent } from './../../../shared/manage-car-objects/manage-car-objects/manage-car-objects.component';

@Component({
  selector: 'app-stereos',
  templateUrl: './stereos.component.html',
  styleUrls: ['./stereos.component.css']
})
export class StereosComponent implements OnInit {
  public selected;
  public data;
  public configuration;

  public loading: boolean;
  public recordId: string;
  public recordName: string;
  public newRecordName: string;
  public form: any;

  public modal = false;

  public columns = [
    { key: 'StereoId', title: 'Id del Equipo' },
    { key: 'Name', title: 'Nombre del Equipo de Sonido' },
    { key: '', title: '' }
  ];

  constructor(
    private stereoService: StereosService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.configuration = EasyTableConfigService.config;
    this.configuration.isLoading = true;
    this.configuration.rows = 10;
    this.configuration.paginationRangeEnabled = true;
    this.loadData();
    this.form = new FormGroup({
      recordname: new FormControl('', [
        Validators.required,
        Validators.minLength(2)
      ])
    });
  }

  loadData() {
    this.loading = true;
    this.stereoService
      .getAllStereos()
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.data = res['body'];
        } else {
          this.data = [];
        }
        this.configuration.isLoading = false;
        this.loading = false;
      });
  }

  editRecord(row) {
    this.recordId = row['StereoId'];
    this.recordName = row['Name'];
    const dialogRef = this.dialog.open(ManageCarObjectsComponent, {
      width: '400px',
      height: '300px',
      data: { action: 'edit', recordName: this.recordName, newRecordName: '' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result['update'] === true) {
        this.updateRecord(result['value']);
      }
    });
  }

  confirmDelete(row) {
    this.recordId = row['StereoId'];
    this.recordName = row['Name'];
    const dialogRef = this.dialog.open(ManageCarObjectsComponent, {
      width: '400px',
      height: '200px',
      data: { action: 'delete', recordName: this.recordName, newRecordName: '' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result['update'] === true) {
        this.deleteRecord();
      }
    });
  }

  saveRecord() {
    this.stereoService
      .addStereo(this.form.get('recordname').value)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open('Información guardada correctamente', 'Aceptar');
          this.form.reset();
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }

  updateRecord(newRecordName: string) {
    this.stereoService
      .updateStereo(newRecordName, this.recordId)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open(
            'Información actualizada correctamente',
            'Aceptar'
          );
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }

  deleteRecord() {
    this.stereoService
      .deleteStereo(this.recordId)
      .toPromise()
      .then(res => {
        if (res['status'] === 200) {
          this.loadData();
          this.snackBar.open('Registro eliminado correctamente', 'Aceptar');
        } else {
          this.snackBar.open(res['body'], 'Aceptar');
        }
      });
  }
}
