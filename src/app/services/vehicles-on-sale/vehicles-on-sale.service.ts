import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from './../../globals';

@Injectable({
  providedIn: 'root'
})
export class VehiclesOnSaleService {
  constructor(private http: HttpClient, private globals: Globals) {}

  getAllVehicleForSale(paramsMap?): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');
    const url = `${this.globals.APIurl}vehiclesonsale/getAllVehicles`;
    if (paramsMap) {
      let params = new HttpParams();
      paramsMap.forEach((value: any, key: any) => {
        params = params.set(key, value);
      });

      return this.http.get(url, {
        headers: httpHeaders,
        responseType: 'json',
        params
      });
    } else {
      return this.http.get(url, {
        headers: httpHeaders,
        responseType: 'json'
      });
    }
  }

  updatePayment(id): Observable<any> {
    const body = new HttpParams().set('VehicleOnSaleId', id);

    return this.http.post(
      `${this.globals.APIurl}vehiclesonsale/updateVehicleIsPayed`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }
}
