import { TestBed } from '@angular/core/testing';

import { VehiclesOnSaleService } from './vehicles-on-sale.service';

describe('VehiclesOnSaleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VehiclesOnSaleService = TestBed.get(VehiclesOnSaleService);
    expect(service).toBeTruthy();
  });
});
