import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from './../../globals';
import { PlanModel } from 'src/app/features/plans/models/plan.model';

@Injectable({
  providedIn: 'root'
})
export class PlansService {
  constructor(private http: HttpClient, private globals: Globals) {}

  getAllplans(): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');

    return this.http.request(
      'GET',
      `${this.globals.APIurl}/plans/getAllplans`,
      {
        headers: httpHeaders,
        responseType: 'json'
      }
    );
  }

  addPlan(planModel: PlanModel): Observable<any> {
    const body = new HttpParams()
      .set('Name', planModel.name)
      .set('TypeOfPost', planModel.typeOfPost)
      .set('MaxPhotos', planModel.maxPhotos)
      .set('Price', planModel.price)
      .set('Featured', planModel.featured);

    return this.http.post(
      `${this.globals.APIurl}plans/addPlan`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }

  updatePlan(planModel: PlanModel): Observable<any> {
    const body = new HttpParams()
    .set('Name', planModel.name)
    .set('PlanId', planModel.planId)
    .set('TypeOfPost', planModel.typeOfPost)
    .set('MaxPhotos', planModel.maxPhotos)
    .set('Price', planModel.price)
    .set('Featured', planModel.featured);

    return this.http.post(
      `${this.globals.APIurl}plans/updatePlan`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }

  deletePlan(spareCategoryId: string): Observable<any> {
    const body = new HttpParams().set('PlanId', spareCategoryId);

    return this.http.post(
      `${this.globals.APIurl}plans/deletePlan`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }
}
