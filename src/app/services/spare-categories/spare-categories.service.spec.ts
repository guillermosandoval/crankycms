import { TestBed } from '@angular/core/testing';

import { SpareCategoriesService } from './spare-categories.service';

describe('SpareCategoriesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SpareCategoriesService = TestBed.get(SpareCategoriesService);
    expect(service).toBeTruthy();
  });
});
