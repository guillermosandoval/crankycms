import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from './../../globals';

@Injectable({
  providedIn: 'root'
})
export class SpareCategoriesService {
  constructor(private http: HttpClient, private globals: Globals) {}

  getAllSpareCategories(): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');

    return this.http.request(
      'GET',
      `${this.globals.APIurl}/sparecategories/getAllSpareCategories`,
      {
        headers: httpHeaders,
        responseType: 'json'
      }
    );
  }

  addSpareCategory(name, icon): Observable<any> {
    const body = new HttpParams().set('Name', name).set('Icon', icon);

    return this.http.post(
      `${this.globals.APIurl}sparecategories/addSpareCategory`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }

  updateSpareCategory(name, icon, recordId): Observable<any> {
    const body = new HttpParams()
      .set('Name', name)
      .set('Icon', icon)
      .set('SpareCategoryId', recordId);

    return this.http.post(
      `${this.globals.APIurl}sparecategories/updateSpareCategory`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }

  deleteSpareCategory(spareCategoryId: string): Observable<any> {
    const body = new HttpParams().set('SpareCategoryId', spareCategoryId);

    return this.http.post(
      `${this.globals.APIurl}sparecategories/deleteSpareCategory`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }
}
