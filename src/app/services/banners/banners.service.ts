import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from './../../globals';

@Injectable({
  providedIn: 'root'
})
export class BannersService {
  constructor(private http: HttpClient, private globals: Globals) {}

  getAllBanners(): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');

    return this.http.request(
      'GET',
      `${this.globals.APIurl}banners/getAllBanners`,
      {
        headers: httpHeaders,
        responseType: 'json'
      }
    );
  }

  addBanner(photo, url): Observable<any> {
    const body = new HttpParams().set('Photo', photo).set('Url', url);

    return this.http.post(
      `${this.globals.APIurl}banners/addBanner`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }

  updateBanners(photo, url, bannerId): Observable<any> {
    const body = new HttpParams()
      .set('Photo', photo)
      .set('Url', url)
      .set('BannerId', bannerId);

    return this.http.post(
      `${this.globals.APIurl}banners/updateBanners`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }

  deleteBanner(bannerId): Observable<any> {
    const body = new HttpParams().set('BannerId', bannerId);

    return this.http.post(
      `${this.globals.APIurl}banners/deleteBanner`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }
}
