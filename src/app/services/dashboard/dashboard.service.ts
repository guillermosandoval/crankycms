import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from './../../globals';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  constructor(private http: HttpClient, private globals: Globals) {}

  getLastTenUsers(): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');

    return this.http.request(
      'GET',
      `${this.globals.APIurl}users/getLatestUsers`,
      {
        headers: httpHeaders,
        responseType: 'json'
      }
    );
  }

  getLastTenVehicles(): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');

    return this.http.request(
      'GET',
      `${this.globals.APIurl}vehicles/getLatestVehicles`,
      {
        headers: httpHeaders,
        responseType: 'json'
      }
    );
  }

  counterVehiculeUtility(): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');

    return this.http.request(
      'GET',
      `${this.globals.APIurl}vehicles/counterVehiculeUtility`,
      {
        headers: httpHeaders,
        responseType: 'json'
      }
    );
  }
}
