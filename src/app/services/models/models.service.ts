import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from './../../globals';

@Injectable({
  providedIn: 'root'
})
export class ModelsService {
  constructor(private http: HttpClient, private globals: Globals) {}

  getAllModels(): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');

    return this.http.request(
      'GET',
      `${this.globals.APIurl}models/getAllModels`,
      {
        headers: httpHeaders,
        responseType: 'json'
      }
    );
  }

  addModel(name, brandName): Observable<any> {
    const body = new HttpParams().set('Name', name).set('BrandName', brandName);

    return this.http.post(
      `${this.globals.APIurl}models/addModel`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }

  updateModel(name: string, modelId: string): Observable<any> {
    const body = new HttpParams().set('Name', name).set('ModelId', modelId);

    return this.http.post(
      `${this.globals.APIurl}models/updateModel`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }

  deleteModel(modelId: string): Observable<any> {
    const body = new HttpParams().set('ModelId', modelId);

    return this.http.post(
      `${this.globals.APIurl}models/deleteModel`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }
}
