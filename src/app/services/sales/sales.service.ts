import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from './../../globals';

@Injectable({
  providedIn: 'root'
})
export class SalesServices {
  constructor(private http: HttpClient, private globals: Globals) {}

  getAllSales(): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');

    return this.http.request(
      'GET',
      `${this.globals.APIurl}/sales/getAllSales`,
      {
        headers: httpHeaders,
        responseType: 'json'
      }
    );
  }

  updateSale(id, val): Observable<any> {
    const body = new HttpParams().set('SaleId', id).set('Payed', val);

    return this.http.post(
      `${this.globals.APIurl}sales/updateSale`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }
}
