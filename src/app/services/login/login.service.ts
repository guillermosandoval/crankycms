import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from './../../globals';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private http: HttpClient, private globals: Globals) {}

  authAdmin(username, password): Observable<any> {

    const body = new HttpParams()
      .set('Email', username)
      .set('Password', password);

    return this.http.post(`${this.globals.APIurl}login/adminAuth`, body.toString(), {
      headers: new HttpHeaders().set(
        'Content-Type',
        'application/x-www-form-urlencoded'
      )
    });
  }

  passwordRecovery(param): Observable<any> {
    const httpParams = new HttpParams().set('Param', param);

    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');

    return this.http.request('GET', `${this.globals.APIurl}login/adminPasswordRecovery`, {
      params: httpParams,
      headers: httpHeaders,
      responseType: 'json'
    });
  }
}
