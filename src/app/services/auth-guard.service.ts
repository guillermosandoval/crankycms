import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(public router: Router, private authService: AuthService) {}

  canActivate(): boolean {

    const isLogged = this.authService.isLogged;

    if (!isLogged) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
