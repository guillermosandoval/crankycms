import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from './../../globals';

@Injectable({
  providedIn: 'root'
})
export class BrandsService {
  constructor(private http: HttpClient, private globals: Globals) {}

  getAllBrands(): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');

    return this.http.request(
      'GET',
      `${this.globals.APIurl}brands/getAllBrands`,
      {
        headers: httpHeaders,
        responseType: 'json'
      }
    );
  }

  addBrand(name): Observable<any> {
    const body = new HttpParams().set('Name', name);

    return this.http.post(
      `${this.globals.APIurl}brands/addBrand`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }


  updateBrand(name: string, brandId: string): Observable<any> {
    const body = new HttpParams().set('Name', name).set('BrandId', brandId);

    return this.http.post(
      `${this.globals.APIurl}brands/updateBrand`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }

  deleteBrand(brandId: string): Observable<any> {
    const body = new HttpParams().set('BrandId', brandId);

    return this.http.post(
      `${this.globals.APIurl}brands/deleteBrand`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }
}
