import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from './../../globals';

@Injectable({
  providedIn: 'root'
})
export class MotorsService {
  constructor(private http: HttpClient, private globals: Globals) {}

  getAllMotors(): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');

    return this.http.request(
      'GET',
      `${this.globals.APIurl}motors/getAllMotors`,
      {
        headers: httpHeaders,
        responseType: 'json'
      }
    );
  }

  addMotor(name): Observable<any> {
    const body = new HttpParams().set('Name', name);

    return this.http.post(
      `${this.globals.APIurl}motors/addMotor`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }

  updateMotor(name: string, motorId: string): Observable<any> {
    const body = new HttpParams().set('Name', name).set('MotorId', motorId);

    return this.http.post(
      `${this.globals.APIurl}motors/updateMotor`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }

  deleteMotor(motorId: string): Observable<any> {
    const body = new HttpParams().set('MotorId', motorId);

    return this.http.post(
      `${this.globals.APIurl}motors/deleteMotor`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }
}
