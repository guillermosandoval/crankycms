import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from './../../globals';

@Injectable({
  providedIn: 'root'
})
export class StereosService {
  constructor(private http: HttpClient, private globals: Globals) {}

  getAllStereos(): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');

    return this.http.request(
      'GET',
      `${this.globals.APIurl}stereos/getAllStereoTypes`,
      {
        headers: httpHeaders,
        responseType: 'json'
      }
    );
  }

  addStereo(name): Observable<any> {
    const body = new HttpParams().set('Name', name);

    return this.http.post(
      `${this.globals.APIurl}stereos/addStereo`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }

  updateStereo(name: string, brandId: string): Observable<any> {
    const body = new HttpParams().set('Name', name).set('StereoId', brandId);

    return this.http.post(
      `${this.globals.APIurl}stereos/updateStereo`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }

  deleteStereo(brandId: string): Observable<any> {
    const body = new HttpParams().set('StereoId', brandId);

    return this.http.post(
      `${this.globals.APIurl}stereos/deleteStereo`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }
}
