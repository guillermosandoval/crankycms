import { TestBed } from '@angular/core/testing';

import { StereosService } from './stereos.service';

describe('StereosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StereosService = TestBed.get(StereosService);
    expect(service).toBeTruthy();
  });
});
