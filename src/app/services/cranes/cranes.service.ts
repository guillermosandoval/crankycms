import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from './../../globals';

@Injectable({
  providedIn: 'root'
})
export class CranesService {
  constructor(private http: HttpClient, private globals: Globals) {}

  getAllPendingCranes(): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');

    return this.http.request(
      'GET',
      `${this.globals.APIurl}cranes/getAllPendingCranes`,
      {
        headers: httpHeaders,
        responseType: 'json'
      }
    );
  }

  getAllApprovedCranes(): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');

    return this.http.request(
      'GET',
      `${this.globals.APIurl}cranes/getAllApprovedCranes`,
      {
        headers: httpHeaders,
        responseType: 'json'
      }
    );
  }

  updateCraneStatus(CraneId, craneStatusId): Observable<any> {
    const body = new HttpParams()
      .set('CraneId', CraneId)
      .set('CraneStatusId', craneStatusId);

    return this.http.post(
      `${this.globals.APIurl}cranes/updateCraneStatus`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }

  deleteCrane(craneId: string): Observable<any> {
    const body = new HttpParams().set('CraneId', craneId);

    return this.http.post(
      `${this.globals.APIurl}cranes/deleteCrane`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }

  updateCraneMembership(data): Observable<any> {
    let params = new HttpParams();
    data.forEach((value: any, key: any) => {
      params = params.set(key, value);
    });

    const headers = new HttpHeaders().set(
      'Content-Type',
      'application/x-www-form-urlencoded'
    );

    return this.http.post(
      `${this.globals.APIurl}cranes/updateCraneMembership`,
      params,
      {
        headers
      }
    );
  }

  updateCrane(data): Observable<any> {
    let params = new HttpParams();
    data.forEach((value: any, key: any) => {
      params = params.set(key, value);
    });

    const headers = new HttpHeaders().set(
      'Content-Type',
      'application/x-www-form-urlencoded'
    );

    return this.http.post(`${this.globals.APIurl}cranes/updateCrane`, params, {
      headers
    });
  }
}
