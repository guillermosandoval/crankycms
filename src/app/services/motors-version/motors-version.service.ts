import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Globals } from './../../globals';

@Injectable({
  providedIn: 'root'
})
export class MotorsVersionService {

  constructor(private http: HttpClient, private globals: Globals) {}

  getAllMotorVersion(): Observable<any> {
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');

    return this.http.request(
      'GET',
      `${this.globals.APIurl}motors/getMotorsVersions`,
      {
        headers: httpHeaders,
        responseType: 'json'
      }
    );
  }

  addMotorVersion(version): Observable<any> {
    const body = new HttpParams().set('Version', version);

    return this.http.post(
      `${this.globals.APIurl}motors/addMotorVersion`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }


  updateMotorVersion(version: string, motorVersionId: string): Observable<any> {
    const body = new HttpParams().set('Version', version).set('MotorVersionId', motorVersionId);

    return this.http.post(
      `${this.globals.APIurl}motors/updateMotorVersion`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }

  deleteMotorVersion(motorVersionId: string): Observable<any> {
    const body = new HttpParams().set('MotorVersionId', motorVersionId);

    return this.http.post(
      `${this.globals.APIurl}motors/deleteMotorVersion`,
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
      }
    );
  }
}
