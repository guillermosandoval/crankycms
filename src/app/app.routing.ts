import { Routes, RouterModule } from '@angular/router';

// Serices
import { AuthGuardService } from './services/auth-guard.service';

// Components
import { PublicContainerComponent } from './public-container/public-container.component';
import { PrivateContainerComponent } from './private-container/private-container.component';
import { LoginComponent } from './features/login/login/login.component';
import { PassRecoveryComponent } from './features/login/pass-recovery/pass-recovery.component';
import { DashboardComponent } from './features/dashboard/dashboard/dashboard.component';
import { BrandsComponent } from './features/brands/brands/brands.component';
import { ModelsComponent } from './features/models/models/models.component';
import { MotorsComponent } from './features/motors/motors/motors.component';
import { MotorsVersionComponent } from './features/motors-version/motors-version/motors-version.component';
import { StereosComponent } from './features/stereos/stereos/stereos.component';
import { SpareCategoriesCrudComponent } from './features/spare-categories/spare-categories-crud/spare-categories-crud.component';
import { PlansCrudComponent } from './features/plans/plans-crud/plans-crud.component';
import { BannersComponent } from './features/banners/banners/banners.component';
import { SalesListComponent } from './features/sales/sales-list/sales-list.component';
import { VehiclesOnSaleComponent } from './features/vehiclesonsale/vehicles-on-sale/vehicles-on-sale.component';
import { CranesComponent } from './features/cranes/cranes/cranes.component';
import { ApproveCranesComponent } from './features/cranes/approve-cranes/approve-cranes.component';
import { EditCranesComponent } from './features/cranes/edit-cranes/edit-cranes.component';

const appRoutes: Routes = [
  {
    path: '',
    component: PublicContainerComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'passwordrecovery', component: PassRecoveryComponent },
      { path: '', redirectTo: '/login', pathMatch: 'full' }
    ]
  },
  {
    path: '',
    component: PrivateContainerComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'brands',
        component: BrandsComponent
      },
      {
        path: 'models',
        component: ModelsComponent
      },
      {
        path: 'motors',
        component: MotorsComponent
      },
      {
        path: 'motorsversions',
        component: MotorsVersionComponent
      },
      {
        path: 'stereos',
        component: StereosComponent
      },
      {
        path: 'sparecategories',
        component: SpareCategoriesCrudComponent
      },
      {
        path: 'plans',
        component: PlansCrudComponent
      },
      {
        path: 'banners',
        component: BannersComponent
      },
      {
        path: 'sales',
        component: SalesListComponent
      },
      {
        path: 'vehiclesonsale',
        component: VehiclesOnSaleComponent
      },
      {
        path: 'cranes',
        component: CranesComponent
      },
      {
        path: 'cranes-approved',
        component: ApproveCranesComponent
      },
      {
        path: 'edit-cranes',
        component: EditCranesComponent
      }
    ],
    canActivate: [AuthGuardService]
  },
  { path: '**', redirectTo: '/login', pathMatch: 'full' }
];

export const RoutingModule = RouterModule.forRoot(appRoutes);
