import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-private-container',
  templateUrl: './private-container.component.html',
  styleUrls: ['./private-container.component.css']
})
export class PrivateContainerComponent {

  public isLogged: boolean;

  constructor(public authService: AuthService, private router: Router) {}

  logOut() {
    this.authService.isLogged = false;
    this.isLogged = false;
    this.router.navigate(['/login']);
  }
}
