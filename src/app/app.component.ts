import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'crankyCMS';
  public isLogged: boolean;

  constructor(public authService: AuthService, private router: Router) {
    this.verifySession();
  }
  
  verifySession() {
    var token = sessionStorage.getItem("token");

    if (token != undefined) {
      this.isLogged = true;
      this.authService.isLogged = true;
    } else {
      this.authService.isLogged = false;
      this.isLogged = false;
      this.router.navigate(['/login']);
    }
  }

  logOut() {
    this.authService.isLogged = false;
    this.isLogged = false;
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }

}
