import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-manage-products',
  templateUrl: './manage-products.component.html',
  styleUrls: ['./manage-products.component.css']
})
export class ManageProductsComponent implements OnInit {
  public answer: boolean;
  public form: any;
  public recordName: string;
  public icon: string;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ManageProductsComponent>
  ) {
    this.recordName = this.data.recordName;
    this.data.newRecordName = this.data.recordName;
    this.icon = this.data.icon;
    this.data.newIcon = this.data.newIcon;
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      recordname: new FormControl('', [
        Validators.required,
        Validators.minLength(2)
      ]),
      icon: new FormControl('', [Validators.required])
    });
  }

  setAnswer(answer: boolean) {
    this.answer = answer;
    this.close();
  }

  close(): void {
    this.dialogRef.close({
      update: this.answer,
      recordname: this.form.get('recordname').value,
      icon: this.form.get('icon').value,
    });
  }
}
