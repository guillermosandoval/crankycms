import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-manage-car-objects',
  templateUrl: './manage-car-objects.component.html',
  styleUrls: ['./manage-car-objects.component.css']
})
export class ManageCarObjectsComponent implements OnInit {
  public answer: boolean;
  public form: any;
  public recordName: string;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ManageCarObjectsComponent>
  ) {
    this.recordName = this.data.recordName;
    this.data.newRecordName = this.data.recordName;
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      recordname: new FormControl('', [
        Validators.required,
        Validators.minLength(2)
      ])
    });
  }

  setAnswer(answer: boolean) {
    this.answer = answer;
    this.close();
  }

  close(): void {
    this.dialogRef.close({
      update: this.answer,
      value: this.form.get('recordname').value
    });
  }
}
